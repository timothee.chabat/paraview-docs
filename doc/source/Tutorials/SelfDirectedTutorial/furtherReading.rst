.. include:: ../../macros.hrst
.. include:: ../../abbreviations.hrst

.. _chapter:FurtherReading:

Further Reading
###############

Thank you for participating in this tutorial. Hopefully you have learned
enough to get you started visualizing large data with ParaView. Here are
some sources for further reading.

The documentation page on ParaView’s web site contains a list of
resources available for further learning and questions:
https://www.paraview.org/documentation .

The ParaView User's Guide is a good resource to have with ParaView. It provides
many other instructions and more detailed descriptions on many features.
The ParaView guide can be accessed from the ParaView documentation page.

The ParaView Wiki is full of information that you can use to help you
set up and use ParaView: https://www.paraview.org/Wiki/ParaView.

In particular, those of you who wish to install a parallel ParaView
server should consult the appropriate build and install pages:
https://www.paraview.org/Wiki/Setting_up_a_ParaView_Server .

If you are interested in learning more about visualization or more
specifics about the filters available in ParaView, consider picking up
the following visualization textbook: :cite:`vtk2006`.

If you plan on customizing ParaView, the previous books and web pages
have lots of information. For more information about using VTK, the
underlying visualization library, and Qt, the GUI library, consider the
following books have more information: :cite:`vtkUsersGuide2010`, :cite:`QtProgramming`.

If you are interested about the design of parallel visualization and
other features of the VTK pipeline, there are several technical papers
available: :cite:`VisualizationPipelines`, :cite:`Ahrens00aparallel`,
:cite:`largeScaleVisualization`, :cite:`RemoteDataVisualization`,
:cite:`modularVisualizationArchitecture`, :cite:`TimeDependentProcessing`.

If you are interested in the algorithms and architecture for ParaView’s
parallel rendering, there are also many technical articles on this as
well: :cite:`SortLast`, :cite:`ClusterVTK`, :cite:`parallelVolumeRendering`.
