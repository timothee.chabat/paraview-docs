.. include:: ../../macros.hrst
.. include:: ../../abbreviations.hrst

.. _chapter:BeginningGUI:

Beginning: GUI
##############

Introduction
============

This usecase presents a few of the more important |ParaView| GUI features.
A full list of features can be found in the ParaView Guide.

Data can be opened by going to **File → Open**. Example data files can be
downloaded from https://www.paraview.org/Download/.

Customize Settings
==================

|paraview| allows users to customize settings. The most important ones are
found in **Edit → Settings → General**. These are:

-  **Auto Apply** - Also found as an icon below the Macros menu.
-  **Auto Convert Properties** - Automatically call **Cell to Point**,
   **Point to Cell**, or extract components from vectors as needed.
-  **Transfer Function Reset Mode** - when to update the minimum and
   maximum for the **Color Legend**.

  -  **Grow and update on Apply** - default. This means only update
     when told to.
  -  **Grow and update every timestep**. This means to update the
     Minimum and/or Maximum only if the current timestep exceeds
     these numbers. Basically, Add and grow.
  -  **Clamp and update every timestep**. Set the minimum and
     maximum every timestep, from the data this timestep. This is
     not recommended behavior, since it makes comparing frame to
     frame confusing.

-  **Scalar Bar Mode** - just leave this one alone.
-  **Default timestep** -

  -  **Go to first timestep** - default.
  -  **Go to last timestep**

.. figure:: ../../images/Beginning_gui_567.jpg

Information tab
===============

-  Open can.ex2.
-  Turn all of the variables on.
-  Press **Apply**.
-  Open the Information tab.

   -  The first section is the **File Properties** of the dataset.
   -  The second section is **Data Grouping**, which tells you what blocks and sets you have in this dataset.
   -  The third section is **Data Statistics** which tells you:

      -  The number of cells
      -  The number of points
      -  The amount of memory used
      -  The bounds which give the X, Y and Z bounds of the bounding box.
   -  The fourth section is **Data Arrays** which gives information on each variable, including the min
      and max. Note that this is for the current timestep only.
   -  The last section is **Time** which gives a list of all timesteps, with their associated time.

.. figure:: ../../images/Beginning_gui_information_tab.png

Right click menu based commands
===============================

-  Open can.ex2.
-  **Apply**.
-  Right click on the object.

   -  Block Name
   -  Block specific visibility commands
   -  Block specific coloring and opacity
   -  Representation and coloring commands
   -  Link Camera

.. figure:: ../../images/Beginning_gui_onscreenMenu.png

Split windows
=============

-  Open can.ex2.
-  **Apply**.
-  Drag the can around with the left mouse button until you can see the can.
-  **Split screen vertical** – i.e., one above the other. This is the
   little box with the horizontal line.

.. figure:: ../../images/Beginning_gui_1.png

-  Select 3D View.
-  Turn the eyeball on for can, in the Pipeline Browser.

-  **Split screen vertical** – i.e., one above the other. This is the
   little box with the horizontal line.

.. figure:: ../../images/Beginning_gui_1.png

-  Select 3D View.
-  Turn the eyeball on for can, in the Pipeline Browser.

.. admonition:: **Did you know?**
   :class: tip

   You can always **Undo** using |pqUndo| icon or **Redo** using |pqRedo| icon.

-  This last was a mistake. **Undo, Undo, Undo**.


-  Select the bottom viewport.
-  **Split screen horizontal** – i.e., one next to the other. This is
   the little box with the vertical line.

-  Select **3D View**.
-  Turn the eyeball on for can, in the Pipeline Browser.

-  Now lets link the cameras.
-  Right click on the upper window.
-  Click on **Link Camera**.
-  Click on the lower right window.
-  Do the same between the two bottom windows.
-  Click in the lower left window, set **Coloring** to **DISPL**.
-  Turn on Color Legend Visibility.
-  Click in the lower right window, set **Coloring** to **ACCL**.
-  Turn on Color Legend Visibility.
-  Click in the upper window, set **Coloring** to **VEL**.
-  Turn on Color Legend Visibility.
-  Go to the last frame.
-  Click on **Rescale to Data Range**.
-  Go to the first frame.
-  **Play**.

.. figure:: ../../images/Beginning_gui_3.png

Move/control windows
====================

-  Select the top window.
-  Click on the **Maximize** button of the upper window.

.. figure:: ../../images/Beginning_gui_1.png

-  Click the **Restore** button of the upper window.
-  Next, Using the left mouse button, grab the title bar of the lower
   left window and drag/drop it into the upper window. These two windows
   have now switched places.
-  Finally, grab the divider between the two lower windows and drag it
   left and right. You can also move the divider between the upper and
   lower windows.

Unlink windows
==============

-  To unlink the windows, we use the Link Manager. **Tools → Manage Links**.
-  Select the second link.
-  Click **Remove**.
-  **Close**.
-  Now, grab and rotate can in the three windows.
-  Finally, delete the bottom two windows, using the **Close** button.

.. figure:: ../../images/Beginning_gui_1.png

Control the center
==================

-  Click on the **Show Center** |pqShowCenterAxes|  icon. Notice that this toggles the center cross.
-  Click on the **Pick Center** |pqPickCenter| icon, then select a location on the can.
-  Rotate the can, and notice where it is rotating.
-  Click on the **Reset Center** |pqResetCenter| icon, returning the rotation location
   and center cross to the center of the object.

Auto apply
==========

ParaView now has the ability to auto apply commands. This button |pqAutoApply| is to
the left of the blue question mark, and looks like this:

Properties tab
==============

The **Properties** tab has three buttons on top: 1) **Apply**
button, 2) the **Reset** button and 3) the **Delete** button. The **Reset**
button will undo any Properties tab changes that a user accidentally has
made. Below these three buttons is a search feature. Search will find
Properties tab items, irrespective of them being standard or advanced.
An example would be **Opacity**.

.. figure:: ../../images/PropertiesTabHeader.png

Advanced Properties tab
=======================

The properties tab initially is in standard layout. To get to the
advance layout, click the gear icon. Side and edge sets for the Exodus
reader are found here. Mode shapes are also found on the **Advanced
Properties** tab. The advanced layout icon is |pqAdvanced|.

Copy/Paste/reset/Save parameters
================================

On each section of the Properties tab there are four icons, as follows:

-  Copy the state of that section of the **Properties** tab to the clipboard.
-  Paste the state from the clipboard to this section of the properties
   tab. This allows you to copy and paste state between filters.
-  Reset to factory defaults.
-  Save this state as the user default.

.. figure:: ../../images/Beginning_gui_SaveParams.jpg

Move the camera
===============

ParaView allows the user to change and store the position of the camera.
Such controls as **Roll**, **Elevation** and **Azimuth** are available.
The **Adjust Camera** |pqEditCamera| icon is on the left side of the row
of icons at the top left of the 3d window.

The **Adjust Camera** dialog box looks like this:

.. figure:: ../../images/Beginning_gui_21.png

Useful controls that I often use (in order) are as follows:

-  **Custom Configure** - Save up to 4 camera positions.
-  **Azimuth** - rotate around the vertical axis. Be sure to hit the
   button after entering a number.
-  **Elevation** - rotate around the horizontal axis in the plane of the
   screen.
-  **Roll** - rotate around the axis coming out of the screen.
-  **View angle** - basically a zoom in.
-  **Camera position** - where the camera is.
-  **Focal point** - where the camera is looking.
-  You can always recenter the object using the **Reset** icon. First,
   however, be sure to change **View angle** back to 30.

Matplotlib characters
=====================

-  If needed, open can.ex2 and read in all of the variables.
-  Select the **DISPL** variable.
-  Move forward one time step using the **Next Frame** icon.
-  We want to add an alpha character after DISPL.

   -  Open the color editor.
   -  Open the Edit Color Legend icon. It is the little color legend
      with the 'e' over it.
   -  Modify the Title **DISPL** to say **DISPL $\alpha$**

-  Here is how to change a 2d plot of **EQPS** to **EQPS (uV/m)**

   -  Plot Over Line. Apply. Turn off all variables other than EQPS.
   -  Change the **Legend Name** from **EQPS** to **EQPS ($\frac{\muV}{m}$)**

-  Matplotlib Mathtext formats are described here:
   https://matplotlib.org/users/mathtext.html

Axes Grid
=========

-  Open dataset disk_out_ref.ex2.
-  In the **Properties** tab, scroll down and select Axis Grid.
-  Note that you can edit the Axis Grid attributes.

.. figure:: ../../images/Advanced_gui_54.png
   :width: 1000px

Lighting - Specular
===================

It is possible to change the specular highlights in ParaView. This is on
the Properties tab, about half way down. It is called Lighting:
Specular. Note that reflections can look like the center of the color
map, thus specular highlights are turned off by default.

Slice View and Layouts
======================

-  Open disk_out_ref.ex2.
-  Turn all variables on.
-  **Apply**.
-  Set **Coloring** to **Temp**.
-  Select **Clip**.
-  Turn off the **Show Plane**.
-  **Apply**.

ParaView supports numerous simultaneous layouts, or windows, into your
data.

-  Select the X to the right of Layout #1 (upper left side of the 3d window).

ParaView also supports different views than 3d views. Here is how to
show your data as a slice view.

-  Select Slice View
-  Turn visibility on for disk_out_ref.ex2.
-  Set **Coloring** to **Temp**.
-  Left click in the window, and drag disk_out_ref around.
-  Move the left, upper and right clip planes by dragging the black wedge.

You can intermix different view types.

-  Split horizontal.
-  Turn visibility on for disk_out_ref.
-  Paint by Temp.

You can also connect the cameras for the different views. This can be
done through the **Tools → Add Camera Links** menu.

.. figure:: ../../images/Advanced_gui_55.png
   :width: 1000px

Render View (Comparison)
========================

ParaView can compare different time steps at the same time. This is
called Comparative View.

-  Open can.ex2.
-  All vars on.
-  **Apply**.
-  Split view horizontal.
-  Select **Render View (Comparative)**.
-  Turn on visibility on the can.
-  **View → Comparative View inspector**.
-  Click on the blue **+**. This creates a 2X2 set of views of can.ex2 at
   four different timesteps.

.. figure:: ../../images/Advanced_gui_59.png
   :width: 1000px

Customize Shortcuts
===================

You can create shortcuts to Menu items (such as Filters) in ParaView.

-  **Tools → Customize Shortcuts**.
-  Find Wavelet. If it has a shortcut already, click Clear.
-  Click in the **Press Shortcut** button.
-  Now, select a shortcut, such as **CTRL W**.
-  Close
-  Now, click **CTRL W**, and you have a Wavelet.
-  **Apply**.

.. |pqAdvanced| image:: ../../images/icons/pqAdvanced.svg
   :width: 20px

.. |pqUndo| image:: ../../images/icons/pqUndo.svg
   :width: 20px

.. |pqRedo| image:: ../../images/icons/pqRedo.svg
   :width: 20px

.. |pqAutoApply| image:: ../../images/icons/pqAutoApply.svg
   :width: 20px

.. |pqPickCenter| image:: ../../images/icons/pqPickCenter.svg
   :width: 20px

.. |pqShowCenterAxes| image:: ../../images/icons/pqShowCenterAxes.svg
   :width: 20px

.. |pqResetCenter| image:: ../../images/icons/pqResetCenter.svg
   :width: 20px

.. |pqEditCamera| image:: ../../images/icons/pqEditCamera.svg
   :width: 20px
