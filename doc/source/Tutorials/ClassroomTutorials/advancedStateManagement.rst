.. include:: ../../macros.hrst
.. include:: ../../abbreviations.hrst

.. _chapter:AdvancedStateManagement:

Advanced: State Management
##########################

Introduction
============

This tutorial covers different types of state management.

Save State / Load State
=======================

|ParaView| state files are saved by selecting the menu option **File → Save State**.
These files can be saved as .pvsm files (xml) or .py files (python).
The pipeline, orientation of the data set, and all view windows are saved.
Select **File → Load State** to open a saved |ParaView| state file.

Save Data
=========

This feature will save the data output from the selected source/filter.
The **Save Data** feature is selected from the main menu: **File → Save Data**.
The data type is whatever you chose when you save the data. If a data
type is missing, it probably means you are trying to output data that is
not supported by this dataset format. For example, you need to have
surface data, as triangles, to output .stl files.

**Save Data** can be used to:

-  Create .csv files of your data which can be read into spreadsheets and other programs.
-  Create .stl files. which are often used for additive manufacturing, or animation programs.

   -  Run filters **Extract Surface** then **Triangulate** before saving Data.
-  Create files Houdini can ingest. These are .geo files.

   -  Again, run filters **Extract Surface** then **Triangulate** before saving Data.

Export Scene
============

This feature will save whatever is in the viewport to a file. The Export
Scene feature is selected from the main menu: File → Export Scene.
Again, the data type is whatever you chose when you export scene.

**Export Scene** can be used to:

-  Create .webgl files of what is displayed in the viewport.
   A .html file will be written out, which you can load in a web browser.
   This format allows users to create interactive data products that anyone can load in a web browser.
-  Create .pdf files of what is displayed in the viewport.

Traces
======

|paraview| can now automatically save traces, or macros. This means that
you can start recording a trace, do something, stop recording a trace,
and save this trace into a file. You can then use this trace file as
input to the |pvbatch| program, or as a macro within |paraview|.

Example:

-  **Tools → Python Shell → Trace → Start Trace**.
-  Open disk_out_ref.ex2.
-  Turn all variables on.
-  **Apply**.
-  Select **Clip**.
-  Use **Z normal**.
-  Unselect **Show plane**.
-  **Apply**.
-  Select **Slice**.
-  Unselect **Show plane**.
-  **Apply**.
-  Set **Coloring** to **Temp**.
-  In the Python Shell window, **Stop Trace**.
-  **Save trace**.
-  **Save as macro**.
-  Call it **Super-disk**.
-  **Edit → Delete All**.

Now, lets run the trace.

-  On the Macros toolbar, you will see **Super-disk**. Click it.
